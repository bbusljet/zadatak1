﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Zadatak1.Models;

namespace Zadatak1.Service
{
    public interface IReadRssService
    {
        Task<List<RssData>> ReadAllAsync();
    }
}
