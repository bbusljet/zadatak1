﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using Zadatak1.Models;

namespace Zadatak1.Service
{
    public class ReadRss : IReadRssService
    {
        public async Task<List<RssData>> ReadAllAsync()
        {
            var link = "http://www.bug.hr/rss/vijesti/";
            string httpResult;
            using (var httpClient = new HttpClient())
            {
                var request = new HttpRequestMessage(HttpMethod.Post, link);
                var response = await httpClient.SendAsync(request);
                httpResult = response.Content.ReadAsStringAsync().Result;
            }
            //DtDprocessing is set to ignore,otherwise i get -For security reasons DTD is prohibited in this XML document-
            XmlReaderSettings xmlReaderSettings = new XmlReaderSettings();
            xmlReaderSettings.DtdProcessing = DtdProcessing.Ignore;
            XmlReader xmlRead = XmlReader.Create(new StringReader(httpResult), xmlReaderSettings);
            XDocument xDocument = XDocument.Load(xmlRead);
            
            //using LINQ to filter the elements i want to use
            var filterEntries = from item in xDocument.Root.Elements().Where(i => i.Name.LocalName == "channel").Elements().Where(i => i.Name.LocalName == "item")
                          select new RssData
                          {
                              Title = item.Elements().First(i => i.Name.LocalName == "link").Value,
                              Description = item.Elements().First(i => i.Name.LocalName == "description").Value,
                              PublishDate = Convert.ToDateTime(item.Elements().First(i => i.Name.LocalName == "pubDate").Value)
                          };

            return filterEntries.ToList();
        }

        
    }
}
