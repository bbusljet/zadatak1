﻿using System;

namespace Zadatak1.Models
{   // object definition
    public class RssData
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime PublishDate { get; set; }
    }
    
}
