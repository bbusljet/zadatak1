﻿using Microsoft.AspNetCore.Mvc;
using Zadatak1.Service;


namespace Zadatak1.Controllers
{
    public class HomeController : Controller
    {
        private IReadRssService _read;

        public HomeController(IReadRssService read)
        {
            _read = read;
        }

        public ViewResult Index()
        {
            var model = _read.ReadAllAsync();
            return View(model);
        }
        public IActionResult Error()
        {
            return View();
        }
    }
}
